『哎呀，真是預料之外的展開呢！S班居然被F班壓制！這是要爆大冷門的節奏嗎！？』
『絕不是S班弱，而是F班的學生太強了』
『原來如此！完全不明你在說什麼呢！』

布魯多的比試結束後，邁克爾他們便開始了這樣的對話。
不知不覺，甚至在學生們之中也變成了不可忽視的存在。嘛在聽之前我就已經看到結局了！我可沒在哭哦！
比試結束布魯多回來之後，他跟阿格諾斯面對面，伸出手臂握緊拳頭。

「交給你了。」
「嗚！交給我吧！」

接著，阿格諾斯朝著布魯多的拳頭同樣以拳相碰。
......這算啥。好青春啊。真羨慕。
話說，平時你們明明老是在吵架，結果你們關係不是很好嗎！真不坦率！再多點交流啊！
無意識地，我看向S班那邊，那個總是說些討人嫌的話的老師變得很焦躁。

「不可能不可能不可能不可能！？太，太扯了！絕對是他們在作弊！主持！那些傢伙在作弊啊！」

哦哦。在誣陷我們莫須有的罪名。
S班的老師，正唾沫橫飛地在叫喚。
不過，主持人完全無視他，繼續進行主持。

『那麼，我們馬上開始下一場比賽吧！下場比賽是那個A班的吉奧尼斯選手的哥哥，羅貝魯托選手登場哦！』
『嚯哦。就是在A班對C班的時候，展示了壓倒性實力的那位學生吧？既然是他的哥哥那還真是值得期待啊』

什麼，阿格諾斯的對戰選手，看來是拉傑先生的兒子羅貝魯托。
他是怎樣的學生啊？就在我歪頭苦思的時候，剛回來的布魯多整個臉都黑了。

「糟透了......。」
「哎？為什麼？」
「誠一老師可能不知道，羅貝魯托是S班的頂點────也就是，學院最強啊。」
「嚯哦......。」

拉傑先生的兒子，真是超優秀啊。
在我想著這些的時候，鬥技場上，不知何時站著一個金髮碧眼給人冷靜印象的帥哥。【你們S班究竟有幾個金髮=_=】
怎麼說呢......跟布魯多很相似，該說是王者的氛圍嗎？總之，就跟我這種一般人不同，有著高貴的氛圍。
我們不禁被羅貝魯托的氛圍壓倒，布魯多夾雜著嘆息這麼說。

「而且，羅貝魯托對於我們而言，是極少數能正常地接待我們的人。老實說，很不好對應吧。」
「啊啊──......。」

真的是不走運啊！不，畢竟是比試，如果是阿格諾斯的話絕對不會放水的，但作為對手真是難以應對！畢竟這邊也有不少內情！
阿格諾斯將自己的武器柳釘棒扛在肩上，站到鬥技場中。

「沒想到，居然要跟你戰鬥啊......。」
「......這次，對於我們S班給你們造成不快這件事，表示十分的抱歉。真是對不起啊。」

在跟阿格諾斯對峙的瞬間，羅貝魯托便低下頭。

「我們沒在意......這麼說就是假話了，但我們對你沒有任何怨恨啦。」
「就算你們不介意我個人，可結果而言我沒有抑制住周圍的人也同樣是事實。嘛，雖然我在S班裡也沒有那種立場就是了......。」

羅貝魯托吐出無語和疲勞的嘆息。
明明被大家稱為學院最強，卻不是S班的中心人物也是一件怪事呢。
不，如果是那個S班的話也不奇怪......。

「這種事就不要記在心裡了？比起那種事，我可是滿懷期待能跟你認真的對戰的啊！」
「......是嗎。既然你這麼說的話......我也認真的做你對手吧。」
「哦！正合我意啊！」

聽了二人的對話，兩位主持人都表示吃驚。

『這是怎麼回事呢......看來到了第三組對戰終於能看到神清氣爽的戰鬥呢！』
『再說了，S班究竟聚集了多少性格棘手的人物啊？S班的S難道不是special的S而是性格惡劣的S嗎？』【原文為性惡，羅馬拼音開頭也是S】
『也許是這樣呢！』

你還同意啊！

『這些事暫且不管，讓我們馬上開始比賽吧！羅貝魯托選手對阿格諾斯選手......比試，開始！』

與莉莉小姐的開場白同時，阿格諾斯立馬衝了出去。

「先手必勝！噢啦啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」

看了他那充滿氣勢的起跑，布魯多不禁抱頭。

「那個笨蛋......哪有人面對學園最強毫無計策的突擊啊......！不，也就只有那傢伙一人了......。」

就如布魯多所言，阿格諾斯很有勢頭揮下的柳釘棒，還有朝著羅貝魯托的突擊，都被羅貝魯托輕易躲過，而且還冷靜的使出魔法回擊。

「『雷雨』」

他朝著阿格諾斯深處右手後，雷之槍從空中朝著阿格諾斯，大量地降下來。

「哦哇！？居然是雷電的暴雨！？我該去哪裡避雨啊！」

不是這個問題吧。
話雖如此，雖然有下槍雨這句話，但真要下起雷之槍的話真是頭疼啊。
比起這些，阿格諾斯面對接連襲來的雷之槍，靈活運用著他那身體能力不斷躲避。
......果然，跟布魯多和貝亞德一樣，沒有使用魔法的意思。只要使用魔法的話，即便不依靠身體能力也能對應的......。

「沒想到你居然能全數躲開啊......。」
「因為我深有體會在這之上的攻擊啊！噢啦，別鬆懈小心吃苦頭哦！」
「庫！」

阿格諾斯在躲開雷之雨同時不斷縮短他與羅貝魯托的距離，在撐過雷之雨之後，順勢使勁揮下柳釘棒。
而羅貝魯托使用放在腰間的長劍接下這一擊。
緊接著，阿格諾斯和羅貝魯托就在那裡展開激烈的劍舞。

「挺能幹的嘛！我還以為你只擅長魔法呢！」
「這沒什麼大不了的。不過，看來是你比較擅長近身戰呢。那麼......！」

阿格諾斯和羅貝路德繼續展開猛烈的對打，不斷激撞，但羅貝路德蓄力，將阿格諾斯大大地吹飛。

「什麼！？真是誇張的力氣啊！」

被吹飛的阿格諾斯馬上就調整體勢，可羅貝魯托趁此時拉開距離，把手放在地面上。

「『雷針柱』」

接著，從羅貝魯托的手貼著的地方開始朝著阿格諾斯，尖銳的雷之柱不斷從地面上突起。

「剛才是天上這次是地下啊！？」

就跟剛才一樣，阿格諾斯不斷躲避，但看來比起上空的攻擊，來自地下的攻擊更難躲避，他挨下了幾發雷之柱。

「嘎啊啊啊啊啊啊！」

即便如此，阿格諾斯還是艱難地用柳釘棒防住襲來的雷擊，總算是撐住了。

「可惡......手臂都麻掉了......。」

阿格諾斯的身上冒出了黑煙。

「沒想到就連這個也能防住啊......始終是有點喪失自信了呀。得要更加努力修練才行。」

看到這樣的阿格諾斯，羅貝魯托也忘記了警戒，決意要走向更高處。......這就是真正厲害的傢伙吧。

「這樣我也不能再看情況了......。」
「啊？」
「──----不再留手了。」
「！」

阿格諾斯歪頭後，羅貝魯托一瞬閉上眼睛──----。

「『雷神裝』」
「哈啊！？」

羅貝魯托睜開眼睛的瞬間，由雷製成的鎧甲便纏繞在他的身上。
這身姿神聖而莊嚴，簡直就是觸動男人心弦的帥氣設計。

「超帥啊啊啊啊啊啊啊啊啊啊啊啊！」
「那個笨蛋真是的......。」

阿格諾斯忘記了自己在比賽中絕叫道。太過單純了吧！
雖然我也在內心吐槽阿格諾斯，不過布魯多就直接愁眉抱頭。
話說，那是什麼魔法啊？在雷屬性魔法的列表裡也不存在啊......。
就在我這麼想的時候，腦內久違地響起廣播。

『習得雷屬性原創魔法【雷神裝】』

很好，我知道是我的身體在做多餘的事情了。
不那可是原創魔法啊！？其他人費盡心思編出的魔法這麼簡單的就習得真的好嗎！？
就在我也由於其他的意義上抱頭苦惱的時候，主持人莉莉小姐他們也表示驚訝。

『這，這是怎麼回事呢！？那個身姿！那個魔法！我從來都沒有見過啊！』
『......那個魔法，就如我們所見將雷電纏繞在身上，但那件雷衣看來有著遠超一般鎧甲的防禦力，更重要的是能夠提升自身速度啊』
『邁克爾先生，你瞭解那個魔法嗎！？』
『是啊。那個魔法是【雷女帝】──---艾倫米娜．奇薩．威布魯格。現役S級冒險者的魔法啊』
『哎哎哎哎哎哎哎哎哎哎！？威布魯格也就是......羅貝魯托選手的母親大人！？』
『就是這麼回事吧。話雖如此，沒想到除了那個【雷女帝】還有人能再現那個魔法啊......』

我剛剛也能使用這個魔法了。萬分抱歉。
話說，羅貝魯托的母親......也就是說，拉傑先生的夫人居然是S級冒險者！？換句話說就是變態！？
......不，我也知道自己說了非常失禮的話，但前例實在是太慘了！崗盧崗托先生還有伽思路他們也是啦！
沒想到身處王妃地位卻還在當冒險者......去王城的那時，之所以沒見到也是因為在哪裡冒險的緣故嗎？那也未免太活躍了吧。
在我想著這些事的時候，羅貝魯托苦笑般地說。

「嘛。我還不能像母親大人那樣長時間使用這魔法就是了。不過────。」
「什！？」

一瞬，羅貝魯托的身姿出現搖晃，接下他便以驚人的速度接近阿格諾斯，橫掃一劍。

「茲！？」

以本能察覺到危險的阿格諾斯無意識地轉為防禦姿勢，但還是毫無關係地被盛大地打飛。

「我還有後續哦。」

接下來便是壓倒性的。
不僅是受到全方位的超高速斬擊的集中炮火，期間還不時地使出踢擊和拳頭。
雖然我的眼睛還能跟得上，但老實說其他人根本看不清羅貝魯托的身姿，只看到阿格諾斯自己朝著各種方向被打飛吧。

「────！」

斬擊的餘波削掉了地面，由此可以看出每一發都蘊含著絕大的威力。
在這之中，阿格諾斯根本沒有反擊的空閒，只是單方面的被打著。
看了就覺得心痛，阿格諾斯身上的傷口不斷增加。
已經到了，何時倒下也不奇怪的地步。
然而────。

「庫！」

阿格諾斯沒有倒下。
倒不如說反而燃起了鬥志，雙眼閃閃發亮，靜靜地忍耐著攻擊。
看到他這個樣子，本應是佔據上風的羅貝魯托露出焦急的表情。

「這樣也還不倒下嗎......！」
「我可沒接受過軟弱的鍛鍊方法啊！」
「是啊......那麼────！」

突然，羅貝魯托背後出現了大量的雷之槍。

「我已經沒有餘力了。這就是我的全力......！」
「很好.........看我從正面將你打倒！」

阿格諾斯即便步伐已經搖搖晃晃，可還是迎面架起柳釘棒。

「『雷極槍』！」

接著，大量的雷之槍，一口氣釋放出來，合成一個軌道，最後化為極大的一根雷之槍。
這把槍，如同要將阿格諾斯貫穿一般，以壓倒性的速度飛翔。
對此，阿格諾斯則────。

「認真的認真的認真的認真的認真的認真的認真的認真的認真的認真的認真的一擊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」

全力的高揮棒，極大的槍和柳釘棒正面衝突。
類似強勁力量的奔流那樣的東西，由柳釘棒和極大的槍衝突的部分釋放出來，僅僅是用肉眼一看就知道施加了無比強大的力量。
阿格諾斯以吐血般氣勢大喊。

「氣勢......不夠啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」

居然，漸漸地阿格諾斯將槍壓回去了────。

「真的是很強的氣勢啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」

終於，阿格諾斯將雷之槍斬落。話說你語言水平真糟糕啊。

「噠啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」
「哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦！」

乘著這個氣勢，阿格諾斯逼近羅貝魯托。
羅貝魯托也不知道是不是如他所言使盡了餘力，雷之鎧消失了。
即便如此，羅貝魯托也絞盡氣力，大吼。
接著────。

「......。」
「......。」

阿格諾斯的脖子，羅貝魯托的脖子，互相的武器都架在上面。
會場全體被沉默籠罩。
最先回神的莉莉小姐宣告勝負。

『哈！？這，這是......平手！這是平手啊！』

居然，兩人的武器刺向脖子時機，由我看來也是全完相同。
也就是說，這次是平手。
聽了主持人的宣告，兩人靜靜地放下武器。

「平手嗎......不，如果你能使用魔法的話，結果就不一樣了吧。」
「不？這就是我的全力了。雖然魔法也是一種力量，但我的最強，果然還是這身歷經磨練的身體啊。」
「......是嗎。不管怎麼說，真是場愉快的戰鬥啊。謝謝。」
「哦！我也很享受啊！」

這麼說完，兩人就在鬥技場上握手。
......不過，這就確定了F班和S班打成平手了。

『那個......由於這場比試平手......如果F班下一場沒有出場選手的話，按人數的規則，將會是平手』
『呼姆......由於人數不足，F班明明贏了兩場也算平手也是一件怪事啊。嘛五人中兩人獲勝，一人打平的話也就這麼回事吧』

聽了主持人這樣的對話，不知為何S班上的一位學生站到了鬥技場上。
仔細一看，總覺得那人跟雷歐的氛圍有點相似。
那位學生，看著我們這邊，鄙視地說。

「明明那裡還有一人能夠戰鬥，這就算平手你們是笨蛋嗎？這應該視為放棄了整個比試。」

我有點聽不明他在說什麼啊。
假使雷歐出場了，如果雷歐贏了的話，S班可就確定輸了啊......。
倒不如說，既然按這個情況繼續的話S班就無條件平手啊，現在都輸了兩局，別作死不是更好嗎？

「還是說怎樣？你該不會想說你無法戰鬥吧？不，有可能啊！畢竟是那個無能的F班啊！？之前的比試也是一樣。我們可是出於善心為了無法使用魔法的你們放水了啊！你們這種貨色只要我們拿出真本事一瞬就能將你們打垮。始終，你們就是一群無法使用魔法，努力也得不到回報的雜魚啊！？」

不，你真的看了之前的比賽嗎？
即便不是認真，結果被打倒的話還是沒有意義啊，更重要的是被無能打倒的其他學生算什麼啊？
嘛，這也只是在煽動我們而已吧。
布魯多他們聽了那個學生的話也皺起了眉什麼都沒說。
我開始有點佩服他居然能說出這麼一大堆耍我們的話了。

「......。」
「哎，雷歐？」

突然，雷歐站了起來。
