梅雨鋒面似乎決定稍事休息，讓星期三成了久違的大晴天。

直到第六堂課結束，天空還是只飄著幾朵積云，春雪背對開始傾斜的太陽，快步走向高圓寺車站。

他的目的地當然是東京另一邊——墨田區押上的新東京鐵塔，正式名稱為“東京通天樹”。約兩小時後，通天樹的特別展望台上將開啟通往太空電梯“赫密斯之索”的傳送門……至少有這個可能性。

整件事源自春雪那幾近痴人說夢的突發奇想。事實上，當他去觀看杉並區與新宿區的對戰時，也沒有任何一個超頻連線者談到赫密斯之索的傳聞。就連推測傳送門開通地點與日期時間的黑雪公主，也在兩天前的全感覺通話結束之際補上一句：“就算撲了個空，你也別太失望啊。”

既然如此，至少也可以順便來一趟平常不會做的“東京東區遠征”……遺憾的是拓武跟千百合都要參加社團，黑雪公主這陣子也為了準備校慶而成天待在學生會，春雪自己又沒有膽量在陌生的區域找人單挑。

“……算了，要是真的撲空，就去逛逛秋葉原的老遊戲專賣店好了。”

於是，春雪只好落寞地這麼自我安慰，坐上中央線電車。

他在錦系町車站換搭半藏門線，於押上車站下車，此時街景已經染上晚霞的色彩。

在人行道上打轉的春雪看到空中那物體時，立刻發出了讚嘆聲。

在東京住久了，反而不會去逛所謂的“東京名勝”，所以這回是他第二次到東京通天樹。桁架結構的巨大高塔在夕陽照耀下發出金色光輝，尖銳聳立的模樣就像座直通天上的梯子。

建築全長六百三十四公尺，底部邊長七十公尺。儘管建設完成以來已經過了三十五年，這座電波塔至今仍是全日本最高的建築物。春雪在原地看著這雄偉的模樣看得出神，好一會兒之後才趕忙走向高塔，在入口處付了國中生的票價，搭上高速電梯。

電梯包廂伴隨著強勁的加速度上升，令春雪全身立刻籠罩在一種與在虛擬世界垂直起飛時不太一樣的昂揚感中，於是他跟前天坐電梯直上都廳展望台時一樣，不由得整個人趴到玻璃牆面上。要是千百合在身邊，肯定會用不敢領教的語氣說：“你還真喜歡高的地方呢！”

十幾秒後，電梯抵達展望台，吐出春雪跟其他幾名觀光客。

春雪忍著想立刻跑向窗邊的衝動，先掃視周圍。由於現在是平日傍晚，整層樓看不到幾個未成年身影。就算有，也只是狀似約會的大學生或父母帶來的幼兒。乍看之下，不曉得有什麼目的，獨自一人站在原地的中學生——也就是“比較像超頻連線者的人”——目前並沒有出現在視野之中。

當然他也可以將神經連結裝置連上設施內的區域網路，然後加速查看對戰名單，但在封閉式網路內做出這樣的舉動，多少有些暴露現實身分的風險。而即使在名單上找到其他超頻連線者的名字，唯一能做的事也只有找對方對戰，但這並非他今天來這裡的目的。

因此春雪只用視線掃瞄過寬廣的展望台，之後就往西側窗邊走去。

儘管高度絕對比不上新宿都廳展望台，然而傍晚天空下那一望無際的首都景色，仍有著壓倒性的震撼力。整個平面上鋪滿了米粒般細小的小型建築物，四處還可以看到有高樓大廈鶴立雞群地突起，仿彿成了一塊老式的電路板。

視線筆直朝前望去，能看到富士山壯麗的輪廓淡淡地出現在魔都彼方。

左上方則可以看到慢慢朝地平線落下的太陽，外圍還有著黑黑的雲層，想來明天多半又會下雨。

春雪的臉繼續往上抬，讓從橘紅轉為淡紫的天空色彩占滿整個視野。噴射機的防撞燈眨眼似的閃爍，遊覽飛船則悠哉悠哉地飄在空中。

——現在這一瞬間，全長四千公里的人工物體正以10馬赫的極音速從遙遠的高空接近。

想到這裡，春雪發出讚嘆的聲音。

——世界是那麼寬廣、那麼巨大。一切都太巨觀了。

我之所以喜歡仰望天空，一定就是想嘗嘗這樣的感覺。想嘗嘗這種又胖又自卑的自己相對變得微觀，變得一點都不重要的感覺。說來，這應該是種逃避現實的行為。

變成Silver Crow飛行時一定也是。那一瞬間，我能用全身感受到加速世界的無邊無際。跟加速世界裡空間與時間上的“無限”相比，就連我那一大堆煩惱，也不過是地面上一閃即逝的火花。只有在接觸到天空時，我才能相信這一點。

可是……

那麼你之前，不，為什麼到現在你仍然向往天空呢？你應該跟我不同，不是為了追求那種剎那的解放感。如果只是如此，憑你現有的能力也很夠。到底為什麼……天空中，到底有什麼你想要的東西……？

春雪心中低語的疑問，當然是針對另一位“師父”Sky Raker所發。

而春雪也隱隱約約猜測過這個答案。當然他不知道這答案正不正確……不，這問題沒有對錯可言。只有當Raker再次張開自己的雙翼飛上天空，答案才會出現。

所以，現在春雪來到通天樹等待傳送門出現的舉動，也許完全是白費工夫。一旦Raker用她一貫的平靜笑容搖搖頭說：“我不打算去。”那麼事情就到此結束。

但春雪認為這樣不對。無論內心有多麼深切的傷痛，SkyRaker終究是一名超頻連線者。既然如此，一旦知道加速世界裡出現全新空間，而且還是座架往天空長達四千公里的橋，必定會不由自主地興奮起來。

就跟此刻心臟不由得越跳越快的春雪一樣。

他看著傍晚的東京都心景色出神，不知不覺時間已經過了五點半。黑雪公主精確預測出的傳送門出現時刻是五點三十四分四十二秒。這一瞬間，就是以赤道為中心沿波狀軌道飛行的赫密斯之索最接近東京的時刻。

春雪心浮氣躁地等了幾分鐘，在預定時刻的五秒前將神經連結裝置連上全球網路。

他在三秒前深吸一口氣，並於兩秒前用力閉上雙眼。一秒前，再以只有自己聽得見的音量喊出：

“超頻連線！”

啪——————！

熟悉的加速聲響拍動全身。

他緩緩睜開眼一看，四周已凍結成藍色的“起始加速空間”。舉凡窗外的都市景色、展望台的地板與柱子，以及三三兩兩的觀光客，全都成了透明的水晶狀物體靜止不動。

春雪以粉紅豬虛擬角色的模樣，悄悄離開自己在現實世界中的身體。他先往後退了兩步，接著猛然轉過身去。

寬廣的展望台正中央，本來是個集合了咖啡廳與紀念品商店的空間，然而現在這些攤位全都消失得無影無踪，只剩一大片空曠的平面。

不管怎麼用力看都沒用，別說傳送門了，連個開關都不存在。春雪在原地呆站了將近十秒，這才吐出一口憋了許久的氣。

——看來“宇宙場地”果然只是個幼稚的空想啊。

正當春雪在心中自言自語，即將癱坐在地之際。

忽然一陣強烈的光芒與震動聲響打在虛擬角色身上，讓春雪不由得跳了起來。他抬頭一看，發現寬廣的平面正中央開始涌出一個巨大的物件。

畫出寬大弧線的樓梯一階階從地板上隆起。圓形舞台則由彼端旋轉登場，還有六根較細的圓柱排成正六邊形屹立不動。

六根透明柱子裡封有不停脈動的藍色光芒，帶著柱子正中央無數發光粒子往垂直方向同步升起，直衝到天花板附近，發出美麗而燦爛的光輝。

“……這就是……通往赫密斯之索的傳送門……”

春雪站起身，以沙啞的聲音自言自語。他已經把不久前的失望忘得一乾二淨，讓豬型虛擬角色用力握緊右手。推測果然沒錯，剛剛是哪個傢伙說這是幼稚的妄想啊？

他就以長著豬蹄的腳奔向階梯，從低沉震動聲嗡嗡作響的六根柱子間穿過，跑向圓形舞台的正中央，沒有一絲畏懼或猶豫。

他並攏雙腳跳出最後一步，但這雙腳卻再也沒有碰到地板。

“嗚哇……？”

春雪看見自己的豬型虛擬角色被分解成無數發光粒子，忍不住叫出聲來。不，這不叫分解，應該稱作還原。這些白色粒子全都是以細小的數位程式碼構成，證明了這是虛擬分身還原成資料數據時所發生的現象。

才剛有這樣的認知，緊接著——

春雪的意識便感覺到自己正以猛烈的速度垂直上升，但起飛時應有的G力卻完全不存在，就只是化為一道沒有質量的光，穿透通天樹頂端朝天空飛去。

視野一片全白。

感覺只中斷了短短幾秒。

首先春雪聽到雙腳碰上平面所發出的堅硬碰撞聲，接著身體的重量忽然恢復，讓他不由得單膝跪地。

他維持蹲低的姿勢，戰戰兢兢地睜開雙眼。

首先看到的，是視野左上方的HP計量表。春雪內心一驚，伸出自己的手端詳一番——那五根發出銳利銀光的手指，毫無疑問屬於熟悉的“Silver Crow”。

明明沒開始對戰卻變成對戰虛擬角色，該不會這裡是沒有規則的“無限制中立空間”？春雪心頭一驚，但隨即發現綠色HP計量表中央顯示著一個英文單字：【LOCKED】。

春雪先是一愣，接著歪頭苦思了半天，決定先不管這件事。他深吸一口氣，抬起臉來。

接著忍不住大喊：

“嗚……哇啊啊啊啊啊！”

上身後仰的力道太強，讓他鏗一聲坐倒在地。但他也沒有意識到姿勢有多難看，只顧著凝視眼前的光景。

春雪所坐的金屬地板只延伸到短短一公尺前，再過去只有天空、白雲，以及下方的地表。

照理說擁有飛行能力的春雪應該早已看慣這景象，然而眼前規模完全不同。因為這裡實在太高了，應該有Silver Crow極限飛行高度的好幾倍……不，應該是好幾十倍。天空染成一片蔚藍，雲層在遙遠的下方排出長條形與巨大漩渦，海是一整片藍，陸地則是模糊的咖啡色與綠色。若從這種地方掉下去，多半在墜地前就會先因為跟大氣摩擦而燒成灰燼。

春雪忍不住蹭著腳步後退，直到遠離了沒有任何東西可抓的邊緣約三公尺，才總算吐出憋了許久的一口氣。接著他縮著身體站起，開始環顧左右。

灰色金屬露台形成寬廣的圓形，目光若沿著線條移動，便會自然地轉過身去。結果——

他看到環狀露台正中央有著彎曲的牆壁。

不，那不是牆壁，是柱子。一根直徑恐怕有一百公尺的巨大柱子垂直往上延伸，而春雪就站在這根柱子底端顯得稍粗一些的環狀部分。

“這就是……赫密斯之索……？”

春雪以耳語般的音量自言自語，茫然地仰望這仿彿諸神居住的巨塔般雄偉的建築物。有如不銹鋼般反射出朦朧光芒的金屬柱子，朝著從蔚藍轉為深藍的天空遠方無限延伸，完全看不見融入景色之中的另一端。

現實世界中的太空電梯，應該是由數根以CNT纖維揉合而成的纜繩搭成，直徑頂多兩公尺左右，與其說是柱子，還不如說是纜繩比較貼切。

但春雪眼前這個由加速世界重現的物體已經不能說是柱子，而是一座直徑一百公尺，浮在數千公里高空的超大規模高塔。到底為什麼要把規模擴大到這種地步呢？

他怎麼想也想不出答案，但現在這些疑問都顯得無關緊要，“宇宙場地”真正存在才是最重要的。不，現在自己所站之處或許應該叫“超高空場地”才對。那麼，是否只要爬上這巨大的高塔，就有真正的宇宙空間等著自己呢……？

“比想像中大得多。”

聽到這個來自右側的聲音，春雪點點頭說：

“是啊……跟這裡比起來，通天樹不過是根牙簽……”

“表層幾乎沒做出任何細節，不知道有沒有內部構造？”

“可是，完全看不到像入口的地方啊……等等。”

春雪全身一顫，表演出僵直身體跳躍的高難度動作，接著右轉九十度大喊：

“哇！是、是是是是是是誰誰誰誰誰啊？”

是誰？什麼時候來的！

他本想犀利地喝問對方是誰，但發出的卻是一串奇妙的聲音。而這個從極近距離低頭看著春雪，面無表情的輪廓——

苗條的暗紅色身體，大腿與下臂十分強健，手上有銳利的爪子，尾巴的動作強而有力。

而且，面罩上有著從後腦突出的三角形耳朵與發出金光的雙眼——錯不了，這是春雪接觸過本人的近戰型對戰虛擬角色中，某位擁有頂級實力的人。

“Pa……Pa、Pard小姐！你、你你你為什麼會在這這這這裡？”

這位“Pard小姐”，也就是紅色軍團“日珥”旗下的六級超頻連線者“Blood Leopard”。她只是輕輕聳肩回答：

“跟你來這裡的理由一樣。”

“咦……”

看到對方若無其事的樣子，總算稍微冷靜下來的春雪才恍然大悟。

傳送到這裡絕非春雪的特權。凡是知道赫密斯之索採用公共攝影機的消息、想到加速世界追加新關卡的可能性，並推測出傳送門出現地點及時機的超頻連線者，全都可以來這裡。

知道有別的玩家跟自己一樣突發奇想，還實際來到這通天樹，讓春雪覺得有點欣慰，朝她露出笑容。但隨即想到另一件事，全身又僵硬起來。

也就是說，隨時都可能有新的虛擬角色陸續出現。春雪趕忙四處張望，但看不出有第三人現身的跡象。

看到春雪事到如今才開始膽顫心驚，Pard小姐有點看不下去地對他說：

“你之所以能第一個進傳送門是因為你太大膽，竟然在展望台加速。我是從樓下的廁所沉潛進來，才會慢了一步。其他人多半是以防止泄漏現實身分為第一優先，得從地面上趕來，所以應該還有幾分鐘的空檔。”

“啊……啊啊，對喔，說得也是……”

春雪現在才為了自己的莽撞而嚇得一身冷汗，並重新好好打個招呼：

“你、你好，午安。”

接著，他對擺開右手的Leopard鞠躬說：

“那個，上次承蒙關照了。事後只寫郵件聯絡沒直接登門道謝，真的很不好意思……”

兩個月前春雪碰上了一個巨大麻煩承蒙Pard小姐大力相助，這番話就是為此道謝。豹頭虛擬角色聳聳肩，以難得的長台詞回答：

“NP。當時你也幫了我們很大的忙，你提供的情報，對於找出秋葉原BG的安全漏洞非常有幫助。話說回來……”

她在春雪背上拍了一記，催促對方移動。

“難得的領先優勢要好好利用。繞柱子一圈檢查看看。”

“K、K是也。”

在自己之後出現的人物，是雖然屬於不同軍團卻關係良好的Blood Leopard，這點讓春雪覺得十分慶幸。換作是Frost Horn，肯定二話不說就從身後抱起春雪往地表摔吧。

兩人穿越寬度約有二十公尺左右的環狀走道，來到赫密斯之索本體所在的柱子前用手實際摸摸看，發出合金般光澤的表面沒有任何變化。雖然上頭有著金屬板接縫之類的細部構造，但實在找不到任何可以用來攀爬的落手處。

Pard小姐以利爪在柱子上一抓，確定柱子硬得抓不出任何痕跡之後，立刻開始往右繞行，春雪也跟了過去。畢竟這根柱子直徑長達一百公尺，光是延著彎曲表面繞一圈都十分費事。好不容易看到傳送位置的另一邊，春雪發現那裡有東西存在，立刻喊道：

“啊……那邊有東西！”

他踩著金屬聲響的腳步跑去。

這種物件乍看之下既像車又像船。全長約六公尺的流線型載具整整齊齊地排列在柱子下方的傾斜台面，指向赫密斯之索的頂端。數量共有十架。

這種載具沒有頂蓋，座位完全采敞篷式設計。最前面是設有透明防風罩的單人用駕駛座，後頭則有兩排兩人座的座位。本體下方裝設有四個作為輪胎的圓盤，想來應該是某種推進裝置。流線型的輪廓實實在在體現出“梭子”的造型。

“這、這什麼玩意兒……”

春雪邊自言自語邊爬上斜台，走近某輛停放在最左端且側面漆有“1”字樣的載具。毫無修飾的鐵灰色機體冰冷地保持沉默，看起來不像有發動。

他戰戰兢兢地伸出手去，戳了戳車門那流線型的形體，緊接著……

輕快的叮咚聲響起，跳出一個紫色的對話框。春雪嚇了一跳，趕忙仔細查看，Pard小姐也從旁把臉湊過來。

視窗最上方以無機質感的字形寫著——

這排文字寫著【3D 18H 25M 18S（JST)】。想來應該是依序標註日、時、分、秒，顯然是某種東西的倒數計時。

“唔，如果說這是在倒數計時，數字會在日本標准時間三天又十八小時二十五分鐘後歸零……正好是星期天正午。”

聽Blood Leopard這麼說，春雪開口問：

“到時會發生什麼事……？”

但Pard小姐沒有回答，而是用她那狀似貓科猛獸的手指朝視窗下方一指，上面也寫著一個短短的句子【DO YOU DRIVE ME?】

更下面只有個寫著YES的按鈕。這句簡單的英文意思是“你要駕駛我嗎？”春雪看得懂，但猶豫著不知道該不該按，結果急躁星人Pard小姐在他耳邊說道：

“你不按我按。”

“啊，我按我按。”

春雪連忙回答，並舉起右手碰向YES按鈕。

霎時，簡短的喇叭音效響起，文字變了。【YOU ARE MY DRIVER !】——你是我的駕駛。

幾秒鐘後，字串又有了改變，只剩下一個單字：【RESERVED】。同時視窗表面慢慢浮現出一個物件。

那是張晶瑩剔透的藍色卡片。上面除了“1”的字樣之外，還顯示著跟眼前視窗同樣的倒數。春雪一拿起這張卡片，最後的現象便隨之發生。

流線型機體應聲從濛濛鐵灰色轉為耀眼的銀色。春雪立刻發現這鏡子般的色澤跟自己，也就是跟Silver Crow的裝甲一模一樣。

“原來如此啊。”

Pard小姐豁然開朗地喃喃自語，走向漆有“2”字樣的飛梭，接著觸摸機體叫出視窗，毫不猶豫地點下YES按鈕。她用兩根手指夾住出現的卡片之後，機體顏色一口氣染成深紅——也就是Blood Leopard的裝甲顏色。

春雪拿著卡片走向Pard小姐，鄭重問道：

“請、請問一下……我跟Pard小姐已經註冊成這不知道是車還是船的駕駛……這點我是隱約看出來了。可是這個倒數計時又是怎麼回事？看起來還有三天以上……”

“簡單，直到星期天正午倒數到零為止，這些飛梭都不會動。”

這明快的回答讓春雪點頭稱是，接著他又涌起下一個疑問：

“這樣啊……可、可是，為什麼要等那麼久……”

他這麼一問，Pard小姐難得張開隱藏在砲彈型面罩下的嘴，露出銳利的牙齒笑說：

“這也很明白。系統給予三天半的時間，是為了讓十架飛梭都能湊齊一名駕駛跟四名乘員。到了星期天正午，我們就要同時踩下油門，朝這根柱子的頂端前進。也就是說……”

深紅的豹頭虛擬角色舉起右手指向遙遠的天頂，以唱歌般的語氣說：

“也就是說，我們得到了參加‘赫密斯之索縱貫賽’的權利。”

春雪花了整整五秒，才聽懂這句話的意思。

“這……也、也就是說，終點是在這座高塔的頂端，也就是，太太太太空？”

春雪以顫抖的聲音問完，Pard小姐裡所當然地點頭回應。

但她還來不及再說什麼，柱子另一頭就接連出現傳送聲。想來應該是一群從地上樓層加速的超頻連線者抵達了展望台上的傳送門。

Pard小姐長尾巴一甩抵在春雪背上，悄悄說道：

“最好趁被發現前快閃。”

的確，飛梭只有十輛，所以頂多只能再讓八個人註冊為駕駛。要是沒搶到資格的人提議用對戰來決定權利歸屬，事情可就麻煩了。

“說……說得也是。”

春雪壓抑住心中的震驚，決定先表示同意。

“停止加速後到地上停車場出口等我，我用機車送你回杉並。”

“咿……”

春雪再度全身僵硬，現實世界中Pard小姐那台大型電動機車的凶暴馬力浮現在他腦海中。

但春雪當然沒有膽量婉拒，只好連連點頭說：

“謝、謝謝謝謝你，這樣可省事了。”

“NP。”

接著兩人異口同聲念出了指令：

“‘超頻離線！’”

所幸——也不知道該說幸運還是遺憾——在現實世界中好久不見的Pard小姐，身上穿的並不是蛋糕店的女僕制服，而是貼身T恤跟窄身牛仔褲。

她穿著寬鬆圍裙時看不出來，但現在貼身布料擠壓出意想不到的份量感，讓春雪不由得視線亂飄。Pard小姐面無表情，從座位下的行李箱拿出預備的安全帽輕輕戴到春雪頭上，接著跨上機車。這次春雪自己系好了安全帽扣，連忙爬上後座，雙手戰戰兢兢環上眼前苗條的腰身。

剛開始他還想放輕力道，但出了通天樹停車場之後，機車的輪內馬達立刻發出咆哮聲，緊接著……

“……啊——！”

春雪還是跟上次一樣，只能在慘叫聲中猛力抓住Pard小姐的身體。只是話說回來，每當遇到紅燈時就會反覆一次那激進到了極點的急停＆加速，讓春雪光是忍耐就已經竭盡全力，根本沒有心思去接收其他的觸覺資訊。

從墨田區經過御徒町、御茶水，來到飯田橋之後，春雪的聽覺接收到了Pard小姐的聲音：

‘離十八點還有五分，有空嗎？’

‘咦，有……有的。’

春雪母親設定的門禁時間是晚上九點，所以還有一段時間。她之所以允許春雪在外頭待到這種對國中生來說不甚恰當的時刻，究竟是出於信任還是單純嫌麻煩不想管，春雪自己也說不準。只要嚴重超過門禁時間一次，就可以從她有沒有生氣來推知這個問題的答案，但春雪當然沒有這種膽量，於是他補上一句說明：‘大概還可以再待兩個小時。’

Pard小姐聽了之後以覺得誇張的語調說：

‘喝個茶喝那麼久，人都會溶掉了。’

咦、喝、喝茶？

還來不及細想，大型機車已經閃動方向燈，騎進一家路旁的速食店。

春雪這八個月來，跟黑雪公主已經來這種店二十次以上，跟Sky Raker也來過一次，但他絲毫沒能習慣這樣的狀況，每次都覺得周圍的人們以寫著“這一點都不登對的組合是怎麼回事？”的視線照射自己，總是直冒冷汗。

春雪努力說服自己那不過是自作多情，根本沒人會去注意其他人在做什麼，同時在四人座上跟Blood Leopard面對面坐下。春雪集中全副精神猛啃她請的照燒漢堡套餐，想藉此從意識中阻隔周圍客人的存在。

眼看這招就要成功，但沒過多久……

Pard小姐從腰包拿出紅色的XSB傳輸線，探出上半身，將一邊接頭往春雪的神經連結裝置上一插，接著面無表情地將另一頭接在自己的裝置上。

就算是視野中出現的有線式連結警告，也遮不住店內多名國高中生顯然正看著自己竊竊私語的光景，到頭來春雪還是縮起脖子，冷汗直流。

直連傳輸線的長度代表兩人交往的進度——Pard小姐顯然根本不在意這種說法，春雪卻沒辦法看得這麼開，忍不住以混著慘叫的思考發聲問道：

‘請、請請請問一下，為為為什麼要直連？’

這答案就再單純不過。

‘因為這樣可以邊吃邊說話。’

春雪唯一能做的就是回答：‘……說得也是。’

Pard小姐說到做到，表演起大口吃漢堡同時經由傳輸線說話的高等技巧。這看起來簡單，但嘴上的動作其實很容易被說話帶動，所以咬到舌頭的風險很高。

‘星期天的比賽，你知道要怎麼沉潛到場地嗎？’

聽她突然這麼問，春雪停下咬著薯條的嘴回答：

‘咦……不是要再跑一趟通天樹的傳送門嗎？’

‘不用這麼麻煩。註冊成駕駛時拿到的卡片，是種叫做“輸送卡”的物品，最多可以同時傳送十個跟你直連的超頻連線者過去。’

‘是、是喔……那我們只要在杉並區集合用這張卡片，就能一口氣抵達赫密斯之索？’

‘Yes。’

這種設計幫了大忙。黑雪公主可是受到生死戰規則束縛的9級玩家，若她也要參加比賽，哪怕只有一瞬間，讓她在遙遠的墨田區連上外界網路仍然太冒險。

春雪鬆了口氣，先咬起漢堡，接著才感受到一個根本性的疑問涌上心頭。從通天樹跳進傳送門直到現在，他一直被狀況牽著走，現在才想到……

‘……話說回來，為什麼突然要辦賽車？那種飛梭應該不是由玩家，而是由系統……也就是BRAIN BURST的管理者方面提供的吧？我當上超頻連線者已經有八個月左右，從來就沒聽說過有這種GM辦的活動……’

Pard小姐想了大約零點五秒之後回答：

‘的確，BB的管理者平常根本不見踪影，可是當加速世界進行大規模升級時，就會像這次一樣舉辦當日限定的活動。舉例來說，前年“東京城堡樂園”開業時……’

東京城堡樂園是座蓋在港灣區的大規模主題樂園。在這個全感覺沉潛技術的全盛期，他們還特意以“真實”為主題，用真正石材建造中古歐洲風的城郭都市，帶來不少話題。

‘……那裡的公共攝影機網路開始上線的當天，加速世界裡舉辦了一個活動，讓玩家設法突破占滿整個都市的大群怪物，朝城堡裡的王座前進。我那隊當時本來快贏了，結果被藍色隊伍拖了大群怪物過來同歸於盡。若下次再遇到Horn那傢伙，我可不會白白放過他。’

看到Pard小姐雙眼燃燒著熊熊烈火，春雪不由得縮起脖子，好不容易才出聲回答：

‘原……原來如此。那這次的比賽也是……也是所謂的“新關卡上線紀念活動”對吧？也就是說，比賽只會舉行一次……？’

‘八九不離十。’

那麼，能搶到僅有十架的飛梭其中之一實在是非常幸運。春雪忍不住在內心裡大喊一聲“Mega lucky！”接著趕緊拋開這個念頭。他之所以前往赫密斯之索，朝太空電梯頂端前進，絕對不是為了搶下公開活動的參賽權，而是無論如何都要把某個想法傳達給她——Sky Raker。

——話是這麼說沒錯，只是……

總不能連基本知識都不知道吧？於是春雪戰戰兢兢地將疑問順著直連線路傳輸過去：

‘Pard小姐……請問一下，既然是比賽，那如果拿到前兩名，是不是會有，這個……’

‘當然有。’

Blood Leopard沒讓春雪說到最後，很乾脆地點點頭。

‘多半有準備相當多的超頻點數，再不然就是有提供強化外裝或其他道具當作獎品。’

‘是……是喔，這樣啊？’

春雪試圖裝作鎮定，喉嚨卻忍不住吞了口口水，讓Pard小姐微微苦笑。她整整齊齊地折好轉眼間已吃完的漢堡的包裝紙，以極為平靜的思緒說：

‘最好別期待太多，數字絕對不會大到能顛覆軍團之間的實力均衡。別說這些了……’

“日珥”的幹部先頓了一頓，甩動辮子問道：

‘你們軍團的五個人全都會參賽嗎？’

‘咦……呃，畢竟飛梭可以坐五個人……’

春雪正要點頭，說到這裡卻突然定格。

即使交情再好，還騎機車接送自己又請吃漢堡，但Pard小姐終究不是“同一國的伙伴”。她是將來難保不會跟黑色軍團敵對的紅色軍團主力成員，讓她知道處於生死戰規則下的黑雪公主動向真的好嗎？

Pard小姐多半立刻看穿了春雪的猶豫，隨即輕輕搖頭。

‘我並不打算趁機拿下黑之王的首級。再說活動期間HP計量表都會鎖定，就算想這麼做也辦不到。’

‘鎖、鎖定……？’

春雪復誦一次，才想起有這麼回事：剛才被傳送到赫密斯之索時，自己的HP計量表上確實清清楚楚地刻著【LOCKED】字樣。

‘呃、呃……也就是說，在比賽期間沒有人會受傷，也沒辦法傷人？’

對這個問題，Pard小姐略加思索，隨即點點頭說：

‘那麼，為什麼飛梭要另外載四個人？我還以為這是為了攻擊或防御其他隊：：’

‘這個問題的答案也是Yes。我想，飛梭本身應該也有設定HP計量表，變成零就會損壞。我剛剛說過的東京城堡樂園活動也是一樣，說得精確點，當時每一隊都會拿到一顆寶珠，得把寶珠帶到城堡最頂樓的王座去。雖然系統上設定玩家本身不會死，但是怪物或其他隊的攻擊卻可以削減寶珠的HP。’

Blood Leopard這番話讓春雪佩服地連連點頭。有這樣的設計，哪怕HP計量表受到保護的情形玩起來多少有點太溫吞，應該還是可以展開一場精彩刺激的比賽。

‘原來如此……是這樣設計的啊，那我想我們軍團應該是五個人都會參加。不過……你為什麼問這個？’

春雪睜大眼睛，眼前的Pard小姐難得欲言又止。

但她的猶豫只維持了短短一秒：

‘加速世界裡，有兩個人對我來說很重要。’

平靜的思念從紅色傳輸線傳來：

‘一個是我的王，我無論如何都要保護她。另一個則是我永遠的勁敵，我倆幾乎同時成為超頻連線者，也交手過不知多少次，人們稱她為“超空流星”、“ICBM”……’

春雪立刻猜到這些綽號指的是誰。

‘……是Sky Raker姐’

Blood Leopard輕輕點頭回答春雪。

‘聽說她回歸時我好高興。可是她只參加領土防衛戰，所以我還見不到她。’

‘啊……對、對喔，說得也是。’

現在紅色軍團“日珥”跟黑色軍團“黑暗星雲”無限期停戰，因此身為紅色軍團主力的Pard小姐自然不能參與進攻杉並區的戰鬥。

春雪深吸一口氣，難得地正視對方的眼睛說話：

‘其實，我也有個理由，無論如何都得帶Raker姐去赫密斯之索。所以就算她沒興趣，我也打算努力說服她。我想到了星期天，你一定見得到她。’

‘是嗎？’

Blood Leopard的回答雖然簡短，嘴邊卻露出淡淡的笑容。她慢慢地、深深地點頭說：

‘謝謝你，Silver Crow，我很慶幸能跟你說這些……搞不好，其實不是兩個人，而是三個人才對。’

——很遺憾，春雪完全聽不懂她這句話的含意，因此想也不想就冒失地問了出來：

‘你、你說什麼？你說三個人怎樣……是說誰？’

Pard小姐很乾脆地拔掉直連傳輸線，仿彿在對他說：‘不告訴你。’

春雪再次搭乘她的機車回杉並區，目送逐漸遠去的車尾燈消失在視野之中，這才模模糊糊地開始思考。

超頻連線者總數約有一千人，幾乎全住在東京。人數多成這樣，實在不可能記住每個人的名字，而且大部分人都只是互相爭奪超頻點數的關係——即使如此，對戰打久了，多半還是會產生“敵對”以外的關係。仔細想想，自己獨一無二的好搭檔拓武——“Cyan Pile ”，剛遇到時也是不折不扣的敵人……

一張張臉孔在春雪腦海中閃過。包括了以黑雪公主為首的軍團同伴、仁子跟Pard小姐、互為好對手的Ash Roller，至於Frost Horn他們勉強也包括在內。

要當上10級超頻連線者，等於要拉下其他超頻連線者。想來這多半正是神秘開發者的意圖：讓一千個年輕人互相鬥爭，從中選出一個最完美的。

然而，即使如此，過程中還是會產生憎恨以外的感情，相信這點就連開發者也沒辦法阻止。Biood Leopard會那麼關心從未待過同一個軍團的SkyRaker，就是最好的證明。

——我也想這樣。

春雪朝自己家走去，心中堅定地想著這個念頭。

——哪怕輸得再慘，懊惱得哭出來，我都絕對不會憎恨對手。因為我愛這個遊戲……我愛加速世界。因為我比任何人都更慶幸自己當上超頻連線者。

——真的？真的只是這樣？

腦子裡突然有著聲音提出質疑。

同時幾個影子迅速在腦中螢幕上閃現。仿彿只用鐵架組合出來的紅銹色虛擬角色、由幾張漆黑薄膜排成的積層虛擬角色，以及——已經永遠消失，有著球面護目鏡與長鈎爪的夜暮色虛擬角色。他們是“加速研究社”的成員，認定BRAIN BURST不是對戰格闘遊戲，而是思考加速工具，只為了獲得與利用點數而活動。

這兩個月來，他們在台面上沒有任何動作，但想來應該沒有就此消滅，而是潛伏在加速世界之中，虎視眈眈地伺機反撲。

——你能原諒那些像伙嗎？他們那樣傷害、折磨你的好友，你能放下對他們的憎恨嗎？

春雪沒有發現，在腦中回蕩的聲音不知不覺間已不再屬於自己，而是種帶有扭曲金屬質感的陰森嗓音。背脊正中央的刺痛感讓他皺起眉頭，踩著暴躁的腳步走進自家大樓之中。

——那種像伙當然該恨，當然該任憑憎恨驅使打垮他們。解放你所有的憤怒、憎恨跟厭惡，破壞一切。你有這個能力。你能扯下他們的手腳，吃他們的肉，喝乾他們的血。沒錯——

吃掉。吃掉。吃掉。吃……

“……少囉嗦！”

春雪深深低頭，壓低聲音大喊。他站在購物商場正門不動，大樓住戶跟來買東西的顧客一臉嫌礙事的表情繞過他。

他覺得自己在無數往左右流開的鞋子裡，看到了發出夜暮般紫色光芒的鈎爪，於是用力閉上雙眼，堅定地告訴自己：

——要是他們再出現，我會跟他們戰鬥。但這不是因為我恨他們，而是因為我愛加速世界，因為我相信裡頭存在著敵意以外的羈絆。我會為了保護這些而戰。

——真的嗎？

說完這句話，那像是金屬彎折聲般的話音就此遠去，背上刺痛也慢慢淡去、消失。

春雪呼出一口長氣，汗濕的手掌在褲子上擦了擦，也不抬起頭來，就這麼踩著沉重腳步往無人的自己家走去。
