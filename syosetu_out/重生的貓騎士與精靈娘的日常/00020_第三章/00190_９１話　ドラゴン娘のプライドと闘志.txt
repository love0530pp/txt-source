「那麼，現在就開始工作了喵！」

皇城的工坊裡，薇兒卡幹勁十足地叫道。


工作台上陳列著用於製作神器的原材料──

奧利哈魯鋼，金剛石，精金以及少量的涅磐鋼等等。


「首先是隊伍的盾，史黛拉醬的武器喵，素材是⋯⋯」

一旦成為一流的鍛造師──能力付與鍛冶師後，都不會再疑慮究竟要製作什麼武器，以及如何製作等等。
薇兒卡分揀出來必要的材料，手法高超地將作業推進下去。

此時，艾麗婭她們──

「馬上開始訓練。史黛拉，過來！」
「了⋯⋯！遵命！」

艾麗婭她們移動到了宅邸的庭院。

朱利烏斯皇子和史黛拉一起走到了庭院的中心。
庭院很大。草坪修整地十分徹底，甚至還配備了泳池。
完全以救世主所住的居所為基準而建造出來的。

訓練開始了，朱利烏斯皇子拿出武器擺好架勢。
他的慣用武器和史黛拉一樣都是大劍。
能看到他用的是訓練用的鈍刀。

相對的，史黛拉使用的是平時的武器。
雖然艾麗婭讓史黛拉用訓練用的武器⋯⋯但朱利烏斯皇子說了沒關係。
艾莉莎也微笑著點頭示意了，就這樣訓練開始了。

「⋯⋯⋯⋯哈！」

史黛拉吐了口氣。面前的是把大劍平舉著的朱利烏斯皇子。

這時史黛拉注意到自己抓著大劍的右手，和拿著大盾的左手在輕微地顫抖著。

（呣⋯⋯毫無破綻的架勢。何等驚人的壓迫力⋯⋯雖然聽聞勇者只有一人，並且衰弱不少，但這也太──）

見到朱利烏斯皇子的架勢後，塔瑪的內心感到十分震驚。
在前世身為用劍的大師的塔瑪看來，朱利烏斯皇子的姿勢，以及壓倒性的氣魄完全超過了人類作為劍士所能到達的極限。

「先熱個身。你想怎麼攻過來都行。」
「唔⋯⋯ 別把我當笨蛋！那個⋯⋯！」

朱利烏斯皇子苦笑著對緊張得身體僵直的史黛拉搭話後，史黛拉通紅著臉回敬給他的同時──以相當猛烈的速度接近朱利烏斯皇子。
接近的同時手臂龍化，尾巴也生長出來。

史黛拉本能上理解到，如果不拿出全力的話，就自己這種人連他的腳邊都難以企及。

「哦，龍人嗎。相當的快啊⋯⋯天真！」
「啊⋯⋯⋯⋯！？」

史黛拉驚得睜大了雙眼。

龍人形態下全身蓄力丟出去的盾牌⋯⋯⋯

被朱利烏斯皇子毫不費力的前踢彈開了。
但是，這並不是半途而廢的史黛拉。

調整了身體的態勢後用右手的大劍瞄準了朱利烏斯皇子的肚子。
猶如把訓練忘得一乾二淨的凶狠的斬擊。


看到那樣的攻擊，艾麗婭，莉莉，菲莉不由得驚聲尖叫。


「喲，從這種姿勢放出斬擊！完美的平衡感！」
「不⋯⋯不可能啊⋯⋯」

史黛拉頓時呆若木雞。她使出的斬擊⋯⋯被朱利烏斯皇子左臂的護手卸掉，從而無效化了。


第一擊用腳，第二擊用手臂⋯⋯⋯

明明是自己全力釋放的攻擊，朱利烏斯皇子卻連劍都不用就防住了。

史黛拉曾在塔瑪的指導下學習大盾和大劍的使用方法以及磨練技術。

但就算這樣⋯⋯⋯


「史黛拉，你的力量很不錯。力氣比我還大。雖然似乎磨練過盾和劍的技術⋯⋯但是動作單純的無可救藥。雖說對魔物的時候行得通，但對上勇者或強大的魔族的話實在是太單純了。」

朱利烏斯皇子對木雞狀的史黛拉繼續說著。
所以才需要在我的訓練下學習技術，然後習得新技能⋯⋯等等。

「新技能嗎⋯⋯？」
「沒錯。不只是提高劍和盾的使用方法，能夠與四魔族匹敵的技能是必要的。通過我的訓練讓你習得技能。因為與強者反覆戰鬥而學會的衍生技能是再適合不過了。」

朱利烏斯皇子對一臉不可思議的史黛拉說道。

正如他所說，這個訓練不只是提高戰鬥技巧，也有習得與四魔族匹敵的衍生技能的目的。

正因為他和艾莉莎過去與四魔族和七大魔王的一部分戰鬥過，所以才能看出艾麗婭和史黛拉的特性，然後強行讓她們習得與其自身戰鬥風格相稱的並能與四魔族匹敵的技能吧。

（新的技能！絶對要學會！然後讓這傢伙認輸！他認輸後，就能得到塔瑪的讚賞了噠！）

自己作為Ｓ級魔物的攻擊被如此輕鬆的防住──

史黛拉被這事實深深地傷害了。
一定要給眼前的男子一點顏色看看。讓親愛的塔瑪稱讚自己是能幹的女人。想到這裡，史黛拉的內心燃起了鬥志。