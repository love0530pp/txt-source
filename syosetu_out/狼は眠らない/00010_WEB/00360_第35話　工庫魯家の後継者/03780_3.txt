３

「老夫過來這裡的來龍去脈，有大致理解了嗎」
「是，是的」
「那麼，就讓老夫轉告亞馬密爾一級神官大人的傳言吧。第一，書的規格會是〈貴典〉。關於〈貴典〉，正如說明，高為三十分，寬為二十一分。六種規格在文字的大小和每頁的文字量上都各有規定。在〈貴典〉的場合，是一行二十六文字，一頁三十六行。也就是基本為九百三十六文字，但會適當地改行，盡可能接近九百字才被視為優美」
「好，好的」
「第二，書的頁數，基本為三百頁到五百頁。較長的原稿會進行分割。沒問題嗎？」
「是，是的」
「此外，相當短的原稿，希望能盡可能將好幾篇彙整起來。沒問題嗎？」
「是的」
「第三，為了把握整個刊行計畫，想先知道原稿的份量。這方面，只要讓老夫看看就可以了。那麼，請讓老夫看看吧」

就算這麼說，也還沒將原稿彙整出一個頭尾。總之，先交出了準備好的『藥草學緒論』的原稿。這下就能爭取兩三天的時間了，諾瑪想著。
但是，諾瑪馬上就了解到，自己輕視了拉庫魯斯的知識和能力。隔天早上，滿滿的寫了用語的書記板被擺到諾瑪眼前。

「拜見完原稿了。內容比原本的書還要充實，容易理解。內容只能以佩服形容」
「不敢當」
「但是關於用語，有些許討論的必要。希望能告訴老夫寫在這裡的用語的意思」
「那個。現在馬上嗎」
「現在馬上。有什麼要事嗎？」
「不。也不是」

經過一時的說明後，受到拉庫魯斯的指謫，讓諾瑪頓悟並感到欽佩。

父親薩斯夫利・瓦茲洛弗開闢的學問領域，是完全嶄新的，還賦予了新的意義給大家通常都知道的詞彙。相對的，也有近似新詞的學術用語，以及完全的新語。
完全的新語大多有給說明，所以還算容易理解，但近似新詞的詞彙不好領會其意義，難以正確地理解。在賦予大家都知道的詞彙新的意義的場合，只閱讀部分時，幾乎都會誤讀。

而且，各地的藥師和施療師，雖然擁有繼承於師的知識，但那知識和詞彙用法在各地也都大有不同。因此，在『藥草學緒論』的場合，如果不好好明確語義，統一用法，在各地就會有不同的解讀法。

拉庫魯斯紀錄在書記板的用語之中，有意義相近但詞彙大有不同的詞彙，也有以別的詞彙表現相同內容的場合。也有明明是同個詞彙卻被用於完全不同之意義的場合。也有明明作為詞彙是相似的，但內容完全不同的場合。

「在這種場合呢，最好把用語分類，以一致的表現來使用同系統的詞彙。然後，意義相同的詞彙，最好盡可能統一為同個用語。畢竟這本書並非文字之書，而是學問之書呢」

因此，拉庫魯斯決定先閱讀幾冊原稿，來為整理用語先行準備。因為只是要整理用語，所以原稿沒有整理也沒關係。

對此，先交出的是『臟腑機能研究』，接著交出的是『藥草學本論』的原稿。『臟腑機能研究』有約九十二萬字，至於『藥草學本論』，是儘管還未完成，但只算完成的部分也超過了三百萬字的大作。而且後者這邊，拉庫魯斯這次應該是第一次看才對。然而，拉庫魯斯僅僅三天就讀完了兩書。

「諾瑪殿」
「是」
「『臟腑機能研究』真是美妙的研究啊」
「非常感謝」
「雖然是以六個章節構成的，但執筆時期是不是相當有差距呢」
「是的。第一章和第六章，差了二十年左右呢。而且，也統合了執筆動機本來就不同的研究」
「啊啊，是二章跟四章吧」
「是的」
「實在美妙的研究啊。不過，內容也有重複，如果要做為一本書來讀，也有前後關係會有點奇怪的地方。另外，似乎是以執筆年代的順序來排列的」
「是的」
「換句話說，不巧的是，並非以內容來排列。當然，諾瑪殿應該很清楚這種事」
「是的」
「要不要果斷地整理原稿並改寫呢」
「⋯」
「還請考慮看看」
「好的」

在拉庫魯斯整理用語集案的期間，諾瑪把想作成書的原稿彙整在一間房間裡。拉庫魯斯的兩位弟子，帕姆和悠蘭非常優秀，若無其事地確切協助了諾瑪。帕姆和悠蘭的動作，是很清楚原稿這種東西的性質的人的動作。這是金格沒有的能力，讓諾瑪欽佩不已。