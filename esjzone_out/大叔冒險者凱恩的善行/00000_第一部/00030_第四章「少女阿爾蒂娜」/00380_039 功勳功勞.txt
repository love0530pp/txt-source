在王都的王宮裏，為了討伐惡神有功者們，舉行了授勳儀式。

現場還有這次的功臣Ｓ級隊伍『綻放在高處的玫瑰少女團』的三位成員和警備隊長奧爾罕的身影。
但是，消滅惡神的最大功臣凱恩卻不見蹤影。
雖然對平民凱恩和冒険家們也會有相應的獎勵，但唯獨凱恩卻沒有被邀請到王宮的授勳儀式上。

所以，劍姫安娜絲托蕾亞的理智斷線了。

「為什麼凱恩的授勳不被認可呢？你不是有叫我參加嗎？」
「即使妳這麼說，授予平民勳章也是史無前例的。」

被劍姫抓住脖子，受到責備的是擔任掌管王国人事的紋章院長官的蒙德伯爵。
擁有絶對權力的王国官僚蒙德伯爵，一旦被劍姫盯上，就如同嬰兒一般。

「你的這個下人真不像話，宰相！」

接著劍姫的矛頭指向了王国宰相麥哲倫。
他被稱為將奧斯托利亞王国引向繁榮的「大宰相」

擁有威嚴的焦茶色八字鬍子的麥哲倫宰相，乾咳一聲後笑瞇瞇地回答道。

「小姐，因為您的功勳功勞已經完全委託給蒙德伯爵了。」
「宰相閣下！啊啊啊啊！！！」

蒙德伯爵發出近似悲鳴的叫聲，麥哲倫宰相因為有事，立刻退出了。
不愧是當之無愧的宰相。
他意識到對方是劍姫是敵不過的，所以交給蒙德伯爵趕快逃走了。

「來吧，我可是聽到了宰相的話了，接下來就只剩下蒙德伯爵你了，至少我要讓凱恩成為子爵。」
「可是！子爵！並沒有把平民封為諸侯的前例。」

退一百步說，即使能授予貴族之位，也沒有子爵。
如果是平民，即使積累再多的功勳，最多也只能成為騎士。
成為騎士的話，連騎士階級最高的準男爵也能升上來。
成為準男爵的話，就能加入貴族的行列。
如果是準貴族的話，也有可能被授予領地的男爵、子爵，但這還需要對王国建立更大的功績。
劍姫說要把這一切都一掃而光的主張，實在是太亂來了。

「前例前例！好吵啊！」
「如果王国有習慣法的規範的話！」
「要想辦法解決這一點，是你們這些官僚的職責不是嗎？」
「這沒什麼。如果王国也能有秩序的話。」

蒙德伯爵拼了命。
掌握王国人事權的蒙德伯爵有很重的責任。
就算被劍姫抓住首級，再怎麼被拖走，也不能讓出這一點。
如果能讓平民突然授勳為貴族，王国的秩序就會崩潰。

「你真是個老腦筋不懂事的人」

蒙德伯爵忍不住想吐嘈出到底是誰才不懂事。
總之，要想在這裏活下去，就只能找人來救他了。蒙德伯爵睜大雙眼尋求幫助。

「那個，安娜絲托蕾亞殿下」
「什麼？」

跟他打招呼的是這次一起授勳的警備隊長奧爾罕。
擁有騎士地位的奧爾罕隊長，以這次的功績登上了準男爵，預定被任命為王国軍的營長。
奧爾罕的手下一百名勇士都會出人頭地。
應該還有警衛隊長或被任命為騎士的人吧。

「在這次的惡神消滅中，凱恩先生有很大的功績，我奧爾罕也看到了。」
「是的，你知道我的事。然後呢？」
「首先，被任命為艾倫城的警備隊長怎麼樣？如果是凱恩先生的實力，我想能很快就會在王国軍隊出人頭地了吧。」
「這樣就好了！奧爾罕你說得真好。安娜絲托蕾亞殿下，只要是警衛隊長的的提議，我沒意見。」

對蒙德伯爵來說，這真是一艘救生船的提案。

「駁回」

但劍姫並不同意。

「這是怎麼回事，唷吚！！」
「你以為要多少年才能在王国軍中出人頭地？我不是叫你快點把凱恩升成子爵嗎！」

再一次被抓到脖子，被吊起來的蒙德伯爵。

「安娜公主，別太過分了！」

該不會真的要殺了他吧。
因為劍姫做得太過分了，魔女瑪雅和聖女賽菲莉亞也會阻止她。

「為什麼！惡神是凱恩打敗他的！別說子爵了，就算成為国王也不奇怪吧！」
「我知道了，你要冷靜一點⋯⋯」

淚目的劍姫，因為力量的不平衡而很可怕。
好不容易才被釋放的蒙德伯爵一邊喊著，一邊在地上爬著逃跑。

「太過分了吧！凱恩拯救了這個国家。連授勳儀式都不叫他，太過分了！」

這時，在劍姫要發狂的時候，蒙德伯爵看到進來的人，大聲叫了起來。

「迪特里希陛下！」

在地上爬來爬去的蒙德伯爵直接跪了下來。
瑪雅們，也急忙低下了頭。
帶來国王陛下的是麥哲倫宰相。
不愧是有本事，原來大宰相並不是只是逃走了。
為了平息事態，只好請示陛下，請求陛下出馬。

迪特里希・奧斯托利亞・亞里歐斯是一位相貌端正的壯年男子，其堂堂的舉止，伴隨著大国之王的威嚴。

「哎呀哎呀，安娜絲托蕾亞，你好像鬧得很兇。」
「叔叔⋯⋯」

就算是劍姫，被迪特里希王高貴的雙眸注視著，也會覺得很不舒服。
迪特里希国王是劍姫亞納斯特雷亞的父親的義兄，也就是劍姫的叔叔。

「安娜絲托蕾亞」我知道你的憤怒，但請不要讓我的家臣為難。」
「但是，每個人都不承認凱恩的功績」

就算是劍姫，也不擅長從小就認識的親戚叔叔。

「我當然承認凱恩先生的功績。報告聽得很清楚。我為了他將功勳相稱的勳章給準備了。」

迪特里希王從懷裏取出的是一枚令人眼花繚亂的緋色大勳章。
緋光勳章是王国最高級別的勳章。
重視王国秩序的蒙德伯爵表示，這是不可以的。

「陛下，恕我直言！為平民授予緋光勳章等，可是史無前例啊。」
「蒙德伯爵，這應該是有給平民的前例吧。如大賢者達納・琳的那個例子。」

聽到這句話，蒙德伯爵立刻注意到。
被稱為王国最厲害，不，全大陸最強的大賢者，也是王的朋友大賢者達納・琳，但不管說多少次，他都不會接受貴族的授勳。
為了表彰這一成就，換成給緋光勳章給大賢者，最後對方才勉為其難的接受了。

国王說過要讓凱恩的待遇跟大賢者達納一樣。

「怎麼樣，安娜絲托蕾亞。你能原諒我嗎？」
「⋯⋯好吧。這一次，我就饒了你。」

當代獲得這枚大勳章的只有大賢者達納。
雖然這次的功績也會授予安娜絲托蕾亞，但是她和凱恩一模一樣很中意的樣子。

「是麼？那太好了。來吧，麥哲倫宰相。在任性的公主的心情還沒有變的時候，完成了授勳儀式吧。」
「遵命」

迪特里希王「哈」地輕快地笑著。
麥哲倫宰相也咧嘴一笑。

「喂，叔叔。請不要取笑我！」
「安娜絲托蕾亞如果你不想被嘲笑，妳會表現得有點像淑女吧？」

就算劍姫平時說只要被嘲笑就會立刻殺了對方，但面對迪特里希王的話，也只能抽一口氣了。
如果不說些苦話，對今後也是沒有好處的。
賢君和有名的国王陛下，懂得如何對待有教養的外甥女。

───

就這樣順利結束了授勳儀式，離開王宮的魔女瑪雅詢問身邊的劍姫。

「可是，為什麼要拒絶大叔的警備隊長職位呢？」
「不需要。別把他當城裏的士兵耍人。凱恩不適合做那樣的工作。」

是這樣嗎？街上的警備隊長是個相當穩定的工作，想要平穩生活的大叔應該會很高興吧。再讓劍姫彎下肚也不好辦，所以決定不管了。
如果是對凱恩的獎勵的話，王国應該也會以金錢的形式給予他。話就這樣圓滿地結束了，但因為瑪雅的壊習慣，有點想取笑他。

「話說回來，妳指責蒙德伯爵是個頑固的人，安娜公主不也拘泥於前例嗎？」
「我哪裏拘泥於前例！」
「因為，妳之所以吵著要讓凱恩的大叔成為子爵，是因為有前例，要和王族的公主結婚，就必須是比子爵更高的貴族了吧？」
「不，不是的！」

突然間，劍姫就像蘋果一樣染上了臉頰，真不知道該怎麼做才好，瑪雅忍不住笑了出來。